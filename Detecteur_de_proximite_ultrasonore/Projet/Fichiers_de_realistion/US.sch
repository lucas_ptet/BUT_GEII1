EESchema Schematic File Version 4
LIBS:US-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PATIENT Lucas | Carte U.S"
Date "2023-11-17"
Rev "1.1"
Comp "IUT GEII Brive"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R8
U 1 1 65293A48
P 2950 2700
F 0 "R8" H 3020 2746 50  0000 L CNN
F 1 "15K" H 3020 2655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2880 2700 50  0001 C CNN
F 3 "~" H 2950 2700 50  0001 C CNN
	1    2950 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 65293AF5
P 2550 3550
F 0 "C6" V 2600 3600 50  0000 L CNN
F 1 "33µF" V 2400 3450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2588 3400 50  0001 C CNN
F 3 "~" H 2550 3550 50  0001 C CNN
	1    2550 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 65293C36
P 1350 3300
F 0 "C7" H 1500 3400 50  0000 R CNN
F 1 "10nF" H 1200 3300 50  0000 R CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1388 3150 50  0001 C CNN
F 3 "~" H 1350 3300 50  0001 C CNN
	1    1350 3300
	1    0    0    1   
$EndComp
$Comp
L power:+12V #PWR0101
U 1 1 6529A7AB
P 2150 2300
F 0 "#PWR0101" H 2150 2150 50  0001 C CNN
F 1 "+12V" H 2165 2473 50  0000 C CNN
F 2 "" H 2150 2300 50  0001 C CNN
F 3 "" H 2150 2300 50  0001 C CNN
	1    2150 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 6529B305
P 3300 2050
F 0 "J1" H 3350 1900 50  0000 R CNN
F 1 "Conn_01x03_Male" H 3406 2237 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3300 2050 50  0001 C CNN
F 3 "~" H 3300 2050 50  0001 C CNN
	1    3300 2050
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT PFM1
U 1 1 652A0B11
P 2950 3100
F 0 "PFM1" H 2880 3146 50  0000 R CNN
F 1 "100K" H 2880 3055 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-V10_Vertical" H 2950 3100 50  0001 C CNN
F 3 "~" H 2950 3100 50  0001 C CNN
	1    2950 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 B2
U 1 1 652F4FD6
P 9200 3550
F 0 "B2" H 9120 3317 50  0000 C CNN
F 1 "Conn_01x02" H 9120 3316 50  0001 C CNN
F 2 "Connector_JST:JST_NV_B02P-NV_1x02_P5.00mm_Vertical" H 9200 3550 50  0001 C CNN
F 3 "~" H 9200 3550 50  0001 C CNN
	1    9200 3550
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0107
U 1 1 6530E735
P 10250 3300
F 0 "#PWR0107" H 10250 3150 50  0001 C CNN
F 1 "+12V" H 10265 3473 50  0000 C CNN
F 2 "" H 10250 3300 50  0001 C CNN
F 3 "" H 10250 3300 50  0001 C CNN
	1    10250 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 65329AC0
P 1500 5600
F 0 "R6" H 1570 5646 50  0000 L CNN
F 1 "2,2K" H 1570 5555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1430 5600 50  0001 C CNN
F 3 "~" H 1500 5600 50  0001 C CNN
	1    1500 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 65329B47
P 1500 6100
F 0 "R7" H 1570 6146 50  0000 L CNN
F 1 "2,2K" H 1570 6055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 1430 6100 50  0001 C CNN
F 3 "~" H 1500 6100 50  0001 C CNN
	1    1500 6100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 65332D01
P 1850 5600
F 0 "J2" V 1956 5413 50  0000 R CNN
F 1 "Conn_01x02_Male" V 2001 5413 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1850 5600 50  0001 C CNN
F 3 "~" H 1850 5600 50  0001 C CNN
	1    1850 5600
	0    -1   1    0   
$EndComp
Wire Wire Line
	1950 5800 2150 5800
$Comp
L Device:R R4
U 1 1 6533E9A8
P 2300 5800
F 0 "R4" V 2093 5800 50  0000 C CNN
F 1 "1K" V 2200 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 2230 5800 50  0001 C CNN
F 3 "~" H 2300 5800 50  0001 C CNN
	1    2300 5800
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J7
U 1 1 6533EB6B
P 1950 6250
F 0 "J7" H 1923 6226 50  0000 R CNN
F 1 "Conn_01x03_Male" H 1923 6271 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1950 6250 50  0001 C CNN
F 3 "~" H 1950 6250 50  0001 C CNN
	1    1950 6250
	1    0    0    1   
$EndComp
Wire Wire Line
	2450 5800 2600 5800
Wire Wire Line
	2150 6250 2150 6300
$Comp
L Amplifier_Operational:TL082 A1
U 1 1 6534F2B1
P 3100 5900
F 0 "A1" H 3400 5950 50  0000 C CNN
F 1 "TL082" H 3350 6050 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 3100 5900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 3100 5900 50  0001 C CNN
	1    3100 5900
	1    0    0    1   
$EndComp
Wire Wire Line
	2800 6000 2600 6000
$Comp
L Amplifier_Operational:TL082 A1
U 3 1 6535D6D6
P 3100 5900
F 0 "A1" H 2700 6200 50  0000 L CNN
F 1 "TL082" H 2650 6100 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 3100 5900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 3100 5900 50  0001 C CNN
	3    3100 5900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0110
U 1 1 65371634
P 3000 5450
F 0 "#PWR0110" H 3000 5300 50  0001 C CNN
F 1 "+12V" H 3015 5623 50  0000 C CNN
F 2 "" H 3000 5450 50  0001 C CNN
F 3 "" H 3000 5450 50  0001 C CNN
	1    3000 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 653772FC
P 4700 6100
F 0 "R5" H 4630 6146 50  0000 R CNN
F 1 "100K" H 4630 6055 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 4630 6100 50  0001 C CNN
F 3 "~" H 4700 6100 50  0001 C CNN
	1    4700 6100
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_POT PG1
U 1 1 6537755F
P 3400 5550
F 0 "PG1" V 3193 5550 50  0000 C CNN
F 1 "500K" V 3284 5550 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-V10_Vertical" H 3400 5550 50  0001 C CNN
F 3 "~" H 3400 5550 50  0001 C CNN
	1    3400 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5900 3700 5900
Wire Wire Line
	3550 5550 3600 5550
Connection ~ 3700 5900
Wire Wire Line
	3700 5900 3900 5900
Wire Wire Line
	3400 5700 3600 5700
Wire Wire Line
	3600 5700 3600 5550
Connection ~ 3600 5550
Wire Wire Line
	3600 5550 3700 5550
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 6538A35D
P 4000 5700
F 0 "J3" V 4106 5740 50  0000 L CNN
F 1 "Conn_01x02_Male" V 4151 5740 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4000 5700 50  0001 C CNN
F 3 "~" H 4000 5700 50  0001 C CNN
	1    4000 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 5900 4250 5900
$Comp
L Diode:1N4148 D1
U 1 1 6538A49A
P 4400 5900
F 0 "D1" H 4400 5800 50  0000 C CNN
F 1 "1N4148" H 4400 6000 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P12.70mm_Horizontal" H 4400 5725 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/1N4148_1N4448.pdf" H 4400 5900 50  0001 C CNN
	1    4400 5900
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 653AAF26
P 5150 6100
F 0 "C5" H 5265 6146 50  0000 L CNN
F 1 "470nF" H 5265 6055 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5188 5950 50  0001 C CNN
F 3 "~" H 5150 6100 50  0001 C CNN
	1    5150 6100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 653B1DAC
P 5400 5700
F 0 "J4" V 5506 5740 50  0000 L CNN
F 1 "Conn_01x02_Male" V 5551 5740 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5400 5700 50  0001 C CNN
F 3 "~" H 5400 5700 50  0001 C CNN
	1    5400 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 5900 6100 5650
Wire Wire Line
	6100 5650 6250 5650
Wire Wire Line
	5400 5900 6100 5900
$Comp
L Device:R_POT PS1
U 1 1 653B97B7
P 5750 5450
F 0 "PS1" H 5680 5496 50  0000 R CNN
F 1 "10K" H 5680 5405 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-H2,5_Horizontal" H 5750 5450 50  0001 C CNN
F 3 "~" H 5750 5450 50  0001 C CNN
	1    5750 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0111
U 1 1 653B9830
P 5750 5250
F 0 "#PWR0111" H 5750 5100 50  0001 C CNN
F 1 "+12V" H 5765 5423 50  0000 C CNN
F 2 "" H 5750 5250 50  0001 C CNN
F 3 "" H 5750 5250 50  0001 C CNN
	1    5750 5250
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL082 A1
U 2 1 653B9995
P 6550 5550
F 0 "A1" H 6800 5650 50  0000 C CNN
F 1 "TL082" H 6750 5750 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6550 5550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 6550 5550 50  0001 C CNN
	2    6550 5550
	1    0    0    1   
$EndComp
Wire Wire Line
	5900 5450 6250 5450
Wire Wire Line
	6850 5200 7000 5200
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 653C53EA
P 7100 5000
F 0 "J5" V 7206 5040 50  0000 L CNN
F 1 "Conn_01x02_Male" V 7251 5040 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7100 5000 50  0001 C CNN
F 3 "~" H 7100 5000 50  0001 C CNN
	1    7100 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 5200 7250 5200
$Comp
L 4xxx:4011 U1
U 3 1 653C9599
P 7650 5200
F 0 "U1" H 7600 5250 50  0000 C CNN
F 1 "4011" H 7600 5150 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7650 5200 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 7650 5200 50  0001 C CNN
	3    7650 5200
	1    0    0    1   
$EndComp
Wire Wire Line
	7250 5200 7250 5100
Wire Wire Line
	7250 5100 7350 5100
Wire Wire Line
	7250 5200 7250 5300
Wire Wire Line
	7250 5300 7350 5300
Connection ~ 7250 5200
Wire Wire Line
	7950 5200 8100 5200
Wire Wire Line
	8100 5200 8100 5450
Connection ~ 8100 5200
Wire Wire Line
	8100 5200 8300 5200
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 653DA46A
P 7900 5450
F 0 "J6" H 8000 5300 50  0000 C CNN
F 1 "Conn_01x02_Male" H 8006 5537 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7900 5450 50  0001 C CNN
F 3 "~" H 7900 5450 50  0001 C CNN
	1    7900 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5550 8100 5800
Wire Wire Line
	8100 5800 8150 5800
$Comp
L Transistor_BJT:BC327 T1
U 1 1 653DED5E
P 8700 5800
F 0 "T1" H 8891 5754 50  0000 L CNN
F 1 "BC327" H 8891 5845 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 8900 5725 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/BC327-D.PDF" H 8700 5800 50  0001 L CNN
	1    8700 5800
	1    0    0    1   
$EndComp
$Comp
L power:+12V #PWR0112
U 1 1 653E3597
P 8800 5550
F 0 "#PWR0112" H 8800 5400 50  0001 C CNN
F 1 "+12V" H 8815 5723 50  0000 C CNN
F 2 "" H 8800 5550 50  0001 C CNN
F 3 "" H 8800 5550 50  0001 C CNN
	1    8800 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 653E3608
P 8800 6200
F 0 "R3" H 8950 6250 50  0000 R CNN
F 1 "1K" H 8950 6150 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 8730 6200 50  0001 C CNN
F 3 "~" H 8800 6200 50  0001 C CNN
	1    8800 6200
	-1   0    0    -1  
$EndComp
$Comp
L 4xxx:4011 U1
U 4 1 653F1714
P 8700 5200
F 0 "U1" H 8650 5250 50  0000 C CNN
F 1 "4011" H 8650 5150 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 8700 5200 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 8700 5200 50  0001 C CNN
	4    8700 5200
	1    0    0    1   
$EndComp
Wire Wire Line
	8300 5200 8300 5100
Wire Wire Line
	8300 5100 8400 5100
Wire Wire Line
	8300 5200 8300 5300
Wire Wire Line
	8300 5300 8400 5300
Connection ~ 8300 5200
Wire Wire Line
	9000 5200 9100 5200
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 653FF9C8
P 9200 5000
F 0 "J8" V 9306 5040 50  0000 L CNN
F 1 "Conn_01x02_Male" V 9351 5040 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9200 5000 50  0001 C CNN
F 3 "~" H 9200 5000 50  0001 C CNN
	1    9200 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 653FFC9B
P 9450 5200
F 0 "R9" V 9350 5200 50  0000 C CNN
F 1 "10K" V 9550 5200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 9380 5200 50  0001 C CNN
F 3 "~" H 9450 5200 50  0001 C CNN
	1    9450 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 65404E09
P 9950 5650
F 0 "R10" H 10020 5696 50  0000 L CNN
F 1 "100K" H 10020 5605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 9880 5650 50  0001 C CNN
F 3 "~" H 9950 5650 50  0001 C CNN
	1    9950 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 5200 9300 5200
Wire Wire Line
	1500 5950 1500 5900
Connection ~ 1500 5900
Wire Wire Line
	1500 5900 2150 5900
Wire Wire Line
	6850 5200 6850 5550
$Comp
L Timer:NE555 IC2
U 1 1 65293EBB
P 2150 3100
F 0 "IC2" H 2100 3150 50  0000 L CNN
F 1 "NE555" H 2050 3050 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 2150 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 2150 3100 50  0001 C CNN
	1    2150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3300 1650 3300
Wire Wire Line
	1450 3300 1450 2600
Wire Wire Line
	1450 2600 2150 2600
Connection ~ 2150 2600
Wire Wire Line
	2150 2300 2950 2300
Wire Wire Line
	2950 3250 2950 3300
Wire Wire Line
	2700 3550 2950 3550
Wire Wire Line
	2950 2850 2950 2900
Wire Wire Line
	2950 2300 2950 2550
Wire Wire Line
	2650 3300 2700 3300
Connection ~ 2950 3300
Wire Wire Line
	2650 3100 2750 3100
Connection ~ 2700 3300
Wire Wire Line
	2700 3300 2950 3300
Connection ~ 2950 2900
Wire Wire Line
	2950 2900 2950 2950
Wire Wire Line
	2750 2900 2750 3100
Wire Wire Line
	2750 2900 2950 2900
Connection ~ 2750 3100
Wire Wire Line
	2750 3100 2800 3100
Wire Wire Line
	2150 2300 2150 2600
Connection ~ 2150 2300
$Comp
L Device:R R1
U 1 1 65460CF7
P 5500 2550
F 0 "R1" H 5570 2596 50  0000 L CNN
F 1 "1K" H 5570 2505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5430 2550 50  0001 C CNN
F 3 "~" H 5500 2550 50  0001 C CNN
	1    5500 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 65460CFD
P 5100 3550
F 0 "C3" V 4940 3550 50  0000 C CNN
F 1 "1nF" V 4849 3550 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5138 3400 50  0001 C CNN
F 3 "~" H 5100 3550 50  0001 C CNN
	1    5100 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 65460D03
P 3850 3300
F 0 "C4" H 4000 3400 50  0000 R CNN
F 1 "10nF" H 3800 3400 50  0000 R CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3888 3150 50  0001 C CNN
F 3 "~" H 3850 3300 50  0001 C CNN
	1    3850 3300
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT PF1
U 1 1 65460D15
P 5500 3100
F 0 "PF1" H 5430 3146 50  0000 R CNN
F 1 "20K" H 5430 3055 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-H2,5_Horizontal" H 5500 3100 50  0001 C CNN
F 3 "~" H 5500 3100 50  0001 C CNN
	1    5500 3100
	-1   0    0    -1  
$EndComp
Text Notes 4600 3150 0    50   ~ 0
IC2\nNE555\n
$Comp
L Timer:NE555 IC1
U 1 1 65460D1C
P 4700 3100
F 0 "IC1" H 4650 3150 50  0000 L CNN
F 1 "NE555" H 4600 3050 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 4700 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 4700 3100 50  0001 C CNN
	1    4700 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3300 4200 3300
Wire Wire Line
	4700 3550 4700 3500
Wire Wire Line
	5500 3250 5500 3300
Wire Wire Line
	5250 3550 5500 3550
Wire Wire Line
	5200 3300 5250 3300
Connection ~ 5500 3300
Wire Wire Line
	5200 3100 5300 3100
Connection ~ 5250 3300
Wire Wire Line
	5250 3300 5500 3300
Wire Wire Line
	5500 2900 5500 2950
Wire Wire Line
	5300 2900 5300 3100
Wire Wire Line
	5300 2900 5500 2900
Connection ~ 5300 3100
Wire Wire Line
	5300 3100 5350 3100
$Comp
L power:+12V #PWR0116
U 1 1 65484AF5
P 3500 1900
F 0 "#PWR0116" H 3500 1750 50  0001 C CNN
F 1 "+12V" H 3515 2073 50  0000 C CNN
F 2 "" H 3500 1900 50  0001 C CNN
F 3 "" H 3500 1900 50  0001 C CNN
	1    3500 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2050 5200 2900
$Comp
L Device:R R2
U 1 1 6552F119
P 8300 5800
F 0 "R2" V 8400 5800 50  0000 C CNN
F 1 "10K" V 8200 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 8230 5800 50  0001 C CNN
F 3 "~" H 8300 5800 50  0001 C CNN
	1    8300 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8450 5800 8500 5800
Text Notes 3250 5300 0    39   ~ 0
Sensibilité
Text Notes 5500 5550 1    39   ~ 0
Seuil
Text Notes 6050 3150 2    39   ~ 0
Fréquence
Text Notes 3250 3100 0    39   ~ 0
Fréquence
$Comp
L US2_Recepteur:US2 Récepteur1
U 1 1 65560772
P 1100 5850
F 0 "Récepteur1" H 650 5750 39  0000 C BNN
F 1 "US2" H 750 5850 39  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric" H 650 5850 39  0001 C CNN
F 3 "" H 650 5850 39  0001 C CNN
	1    1100 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 6350 9200 6350
Wire Wire Line
	2150 6350 2150 6400
Wire Wire Line
	1650 2650 1650 2900
Wire Wire Line
	2700 2650 2700 3300
Wire Wire Line
	2950 3300 2950 3550
Wire Wire Line
	3500 1900 3500 1950
Connection ~ 4700 3550
Wire Wire Line
	4700 3550 4950 3550
Wire Wire Line
	4700 3550 4700 3600
Wire Wire Line
	5500 3300 5500 3550
Wire Wire Line
	4200 2650 5250 2650
Wire Wire Line
	4200 2650 4200 2900
Wire Wire Line
	5250 2650 5250 3300
Wire Wire Line
	9400 3550 9400 3600
Wire Wire Line
	9950 5400 9950 5450
Wire Wire Line
	9950 5450 10050 5450
Connection ~ 9950 5450
Wire Wire Line
	9950 5450 9950 5500
Wire Wire Line
	9950 5850 9950 5800
Wire Wire Line
	9600 5200 9650 5200
Wire Wire Line
	8800 5600 8800 5550
Wire Wire Line
	8800 6000 8800 6050
Wire Wire Line
	8800 6350 8850 6350
Wire Wire Line
	5750 5250 5750 5300
Wire Wire Line
	3700 5550 3700 5900
Wire Wire Line
	2600 5550 2600 5800
Wire Wire Line
	2600 5550 3250 5550
Connection ~ 2600 5800
Wire Wire Line
	2600 5800 2800 5800
Wire Wire Line
	4550 5900 4700 5900
Wire Wire Line
	4700 5900 4700 5950
Connection ~ 4700 5900
Wire Wire Line
	4700 5900 5150 5900
Wire Wire Line
	5150 5900 5150 5950
Connection ~ 5150 5900
Wire Wire Line
	5150 5900 5300 5900
Wire Wire Line
	5750 6300 5150 6300
Wire Wire Line
	4700 6250 4700 6300
Connection ~ 4700 6300
Wire Wire Line
	5150 6250 5150 6300
Connection ~ 5150 6300
Wire Wire Line
	5150 6300 4700 6300
Wire Wire Line
	2150 5900 2150 6150
Wire Wire Line
	2150 6300 2600 6300
Wire Wire Line
	2600 6000 2600 6300
Connection ~ 2600 6300
Wire Wire Line
	2600 6300 4700 6300
Wire Wire Line
	1500 6250 1500 6300
Wire Wire Line
	1200 5900 1500 5900
Wire Wire Line
	5750 5600 5750 6300
$Comp
L power:+12V #PWR0118
U 1 1 6590FC01
P 1500 5400
F 0 "#PWR0118" H 1500 5250 50  0001 C CNN
F 1 "+12V" H 1515 5573 50  0000 C CNN
F 2 "" H 1500 5400 50  0001 C CNN
F 3 "" H 1500 5400 50  0001 C CNN
	1    1500 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 5750 1500 5900
Wire Wire Line
	1200 5800 1850 5800
Wire Wire Line
	1500 5400 1500 5450
Connection ~ 5500 2900
Wire Wire Line
	4700 2250 4700 2300
Wire Wire Line
	4700 2300 4700 2700
Connection ~ 4700 2300
Wire Wire Line
	4700 2300 5500 2300
$Comp
L power:+12V #PWR0119
U 1 1 65460D0F
P 4700 2250
F 0 "#PWR0119" H 4700 2100 50  0001 C CNN
F 1 "+12V" H 4715 2423 50  0000 C CNN
F 2 "" H 4700 2250 50  0001 C CNN
F 3 "" H 4700 2250 50  0001 C CNN
	1    4700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3500 2150 3550
Wire Wire Line
	2150 3550 2400 3550
Connection ~ 2150 3550
Wire Wire Line
	5500 2700 5500 2900
Wire Wire Line
	5500 2400 5500 2300
$Comp
L Transistor_BJT:2N2219 T2
U 1 1 659C2E62
P 9850 5200
F 0 "T2" H 10041 5246 50  0000 L CNN
F 1 "1N2222" H 10041 5155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 10050 5125 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 9850 5200 50  0001 L CNN
	1    9850 5200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 659EE239
P 9950 4900
F 0 "#PWR0121" H 9950 4750 50  0001 C CNN
F 1 "+5V" H 9965 5073 50  0000 C CNN
F 2 "" H 9950 4900 50  0001 C CNN
F 3 "" H 9950 4900 50  0001 C CNN
	1    9950 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED Rouge1
U 1 1 65A1022F
P 9000 6350
F 0 "Rouge1" H 9000 6450 50  0000 C CNN
F 1 "LED1" H 9000 6250 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 9000 6350 50  0001 C CNN
F 3 "~" H 9000 6350 50  0001 C CNN
	1    9000 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	7900 4100 7900 4150
Wire Wire Line
	7900 3100 7900 3050
$Comp
L power:+12V #PWR0103
U 1 1 652EF812
P 7900 4150
F 0 "#PWR0103" H 7900 4000 50  0001 C CNN
F 1 "+12V" H 7900 4300 50  0000 C CNN
F 2 "" H 7900 4150 50  0001 C CNN
F 3 "" H 7900 4150 50  0001 C CNN
	1    7900 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:C C8
U 1 1 652E481D
P 7500 3600
F 0 "C8" H 7600 3500 50  0000 R CNN
F 1 "100nF" H 7350 3600 50  0000 R CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 7538 3450 50  0001 C CNN
F 3 "~" H 7500 3600 50  0001 C CNN
	1    7500 3600
	1    0    0    1   
$EndComp
$Comp
L 4xxx:4011 U1
U 5 1 652DB050
P 7900 3600
F 0 "U1" V 7950 3650 50  0000 R CNN
F 1 "4011" V 7800 3700 50  0000 R CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7900 3600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 7900 3600 50  0001 C CNN
	5    7900 3600
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 6530CB54
P 10500 3600
F 0 "#PWR0105" H 10500 3450 50  0001 C CNN
F 1 "+5V" H 10400 3750 50  0000 L CNN
F 2 "" H 10500 3600 50  0001 C CNN
F 3 "" H 10500 3600 50  0001 C CNN
	1    10500 3600
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 65B5D948
P 1500 6300
F 0 "#PWR0102" H 1500 6050 50  0001 C CNN
F 1 "GND" H 1505 6127 50  0000 C CNN
F 2 "" H 1500 6300 50  0001 C CNN
F 3 "" H 1500 6300 50  0001 C CNN
	1    1500 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 65B79EA8
P 7900 3050
F 0 "#PWR0122" H 7900 2800 50  0001 C CNN
F 1 "GND" H 8000 2900 50  0000 R CNN
F 2 "" H 7900 3050 50  0001 C CNN
F 3 "" H 7900 3050 50  0001 C CNN
	1    7900 3050
	-1   0    0    1   
$EndComp
Connection ~ 7900 4150
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 65B7A218
P 10250 3400
F 0 "#FLG0103" H 10250 3475 50  0001 C CNN
F 1 "PWR_FLAG" V 10250 3550 50  0000 L CNN
F 2 "" H 10250 3400 50  0001 C CNN
F 3 "~" H 10250 3400 50  0001 C CNN
	1    10250 3400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 65B8856F
P 2150 3650
F 0 "#PWR0120" H 2150 3400 50  0001 C CNN
F 1 "GND" H 2155 3477 50  0000 C CNN
F 2 "" H 2150 3650 50  0001 C CNN
F 3 "" H 2150 3650 50  0001 C CNN
	1    2150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 65B9D871
P 9400 3600
F 0 "#PWR0104" H 9400 3350 50  0001 C CNN
F 1 "GND" H 9405 3427 50  0000 C CNN
F 2 "" H 9400 3600 50  0001 C CNN
F 3 "" H 9400 3600 50  0001 C CNN
	1    9400 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 65B9DCB0
P 10150 3700
F 0 "#PWR0106" H 10150 3450 50  0001 C CNN
F 1 "GND" H 10155 3527 50  0000 C CNN
F 2 "" H 10150 3700 50  0001 C CNN
F 3 "" H 10150 3700 50  0001 C CNN
	1    10150 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 65BB300E
P 9950 5850
F 0 "#PWR0108" H 9950 5600 50  0001 C CNN
F 1 "GND" H 9955 5677 50  0000 C CNN
F 2 "" H 9950 5850 50  0001 C CNN
F 3 "" H 9950 5850 50  0001 C CNN
	1    9950 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 65BB30F4
P 9200 6350
F 0 "#PWR0109" H 9200 6100 50  0001 C CNN
F 1 "GND" V 9205 6222 50  0000 R CNN
F 2 "" H 9200 6350 50  0001 C CNN
F 3 "" H 9200 6350 50  0001 C CNN
	1    9200 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 65BE47DC
P 2150 6400
F 0 "#PWR0113" H 2150 6150 50  0001 C CNN
F 1 "GND" H 2155 6227 50  0000 C CNN
F 2 "" H 2150 6400 50  0001 C CNN
F 3 "" H 2150 6400 50  0001 C CNN
	1    2150 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 65BF9AA9
P 3000 6450
F 0 "#PWR0114" H 3000 6200 50  0001 C CNN
F 1 "GND" H 3005 6277 50  0000 C CNN
F 2 "" H 3000 6450 50  0001 C CNN
F 3 "" H 3000 6450 50  0001 C CNN
	1    3000 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 65C00E1C
P 4700 3600
F 0 "#PWR0115" H 4700 3350 50  0001 C CNN
F 1 "GND" H 4705 3427 50  0000 C CNN
F 2 "" H 4700 3600 50  0001 C CNN
F 3 "" H 4700 3600 50  0001 C CNN
	1    4700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2050 5650 2050
$Comp
L 4xxx:4011 U1
U 1 1 65C5AA7E
P 6150 2050
F 0 "U1" H 6100 2100 50  0000 C CNN
F 1 "4011" H 6100 2000 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 6150 2050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 6150 2050 50  0001 C CNN
	1    6150 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2050 5650 1950
Wire Wire Line
	5650 1950 5850 1950
Wire Wire Line
	5650 2050 5650 2150
Wire Wire Line
	5650 2150 5850 2150
Connection ~ 5650 2050
Wire Wire Line
	6450 2050 6450 2250
Wire Wire Line
	6450 2250 7250 2250
Wire Wire Line
	6450 2050 6550 2050
Connection ~ 6450 2050
$Comp
L 4xxx:4011 U1
U 2 1 65C73B43
P 7000 2050
F 0 "U1" H 6950 2100 50  0000 C CNN
F 1 "4011" H 6950 2000 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 7000 2050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4011bms-12bms-23bms.pdf" H 7000 2050 50  0001 C CNN
	2    7000 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2050 6550 1950
Wire Wire Line
	6550 1950 6700 1950
Wire Wire Line
	6550 2050 6550 2150
Wire Wire Line
	6550 2150 6700 2150
Connection ~ 6550 2050
$Comp
L US1_emetteur:US1 Emetteur1
U 1 1 65C81276
P 8000 2100
F 0 "Emetteur1" H 8000 2050 50  0000 L CNN
F 1 "US1" H 8000 2150 50  0000 L CNN
F 2 "Resistor_SMD:R_2010_5025Metric" H 8150 2050 50  0001 C CNN
F 3 "" H 8150 2050 50  0001 C CNN
	1    8000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2050 7600 2050
Wire Wire Line
	7250 2250 7250 2150
Wire Wire Line
	7250 2150 7600 2150
Wire Wire Line
	2150 2600 2150 2650
Connection ~ 7900 3050
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 65D15E9E
P 10150 3600
F 0 "#FLG0102" H 10150 3675 50  0001 C CNN
F 1 "PWR_FLAG" V 10100 3500 50  0000 L CNN
F 2 "" H 10150 3600 50  0001 C CNN
F 3 "~" H 10150 3600 50  0001 C CNN
	1    10150 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 3550 2150 3650
Wire Wire Line
	2150 2650 2150 2700
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 65E4008C
P 10500 3500
F 0 "#FLG0101" H 10500 3575 50  0001 C CNN
F 1 "PWR_FLAG" V 10500 3800 50  0000 C CNN
F 2 "" H 10500 3500 50  0001 C CNN
F 3 "~" H 10500 3500 50  0001 C CNN
	1    10500 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2450 2650 2900
Wire Wire Line
	1650 2650 2700 2650
Wire Notes Line
	9050 3900 9050 3100
Text Notes 9500 3250 0    50   ~ 0
Borniers
Wire Wire Line
	3500 2450 3500 2150
Wire Wire Line
	2650 2450 3500 2450
Wire Notes Line
	3900 1650 3900 2300
Wire Notes Line
	3100 2300 3100 1650
Wire Notes Line
	3100 1650 3900 1650
Wire Notes Line
	3100 2300 3900 2300
Text Notes 3100 1800 0    39   ~ 0
Support de \ncavalier
Text GLabel 9450 3450 2    50   Input ~ 0
Vout
Wire Wire Line
	9400 3450 9450 3450
Text GLabel 10050 5450 2    50   Input ~ 0
Vout
Wire Wire Line
	3500 2050 4000 2050
Wire Wire Line
	4000 2050 4000 3300
Wire Wire Line
	1350 3100 1350 3150
Wire Wire Line
	1350 3100 1650 3100
Wire Wire Line
	1350 3450 1350 3550
Wire Wire Line
	1350 3550 2150 3550
Wire Wire Line
	3850 3150 3850 3100
Wire Wire Line
	3850 3100 4200 3100
Wire Wire Line
	3850 3450 3850 3550
Wire Wire Line
	3850 3550 4700 3550
Wire Wire Line
	7500 3050 7500 3450
Wire Wire Line
	7500 3050 7900 3050
Wire Wire Line
	7500 3750 7500 4150
Wire Wire Line
	7500 4150 7900 4150
Wire Wire Line
	9950 4900 9950 5000
Wire Wire Line
	10100 3500 10500 3500
Wire Wire Line
	10500 3500 10500 3600
Connection ~ 10500 3500
$Comp
L Connector_Generic:Conn_01x03 B1
U 1 1 652F4E7A
P 9900 3500
F 0 "B1" H 9820 3267 50  0000 C CNN
F 1 "Conn_01x03" H 9820 3266 50  0001 C CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 9900 3500 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
	1    9900 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	10100 3400 10250 3400
Wire Wire Line
	10250 3400 10250 3300
Connection ~ 10250 3400
Wire Wire Line
	10100 3600 10150 3600
Wire Wire Line
	10150 3600 10150 3700
Connection ~ 10150 3600
Wire Notes Line
	9050 3100 11000 3100
Wire Notes Line
	11000 3100 11000 3900
Wire Notes Line
	9050 3900 11000 3900
Wire Wire Line
	3000 6200 3000 6450
Wire Wire Line
	3000 5450 3000 5600
Wire Wire Line
	3500 2150 3700 2150
Connection ~ 3500 2150
Text Label 3700 2150 2    50   ~ 0
V1Hz
$EndSCHEMATC
